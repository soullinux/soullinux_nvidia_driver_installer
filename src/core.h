#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <pwd.h>
#include <time.h>
#include <aliases.h>
#include <unistdio.h>
#include <alloca.h>
#include <malloc.h>

#define def int
#define print printf
#define scan scanf
#define is ==
#define linux __linux__
#define elif else if
#define or ||
#define exit_sucess return 0;
#define exit_failure return 1;
#define init_num int argc
#define init_str char * const argv[]
#define num int
#define str char