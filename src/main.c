// (C) Copyright 2022 Venkatesh Mishra
// Soullinux nvidia driver installer

#include "core.h"

def main(init_num, init_str) {
    str kernel[5];
    int short i;
    print("Soullinux Nvidia driver installer\n");
    print("Please enter the kernel you are using (example linux-lts, linux, dkms):\n");
    scan("%s",&kernel);
    if(strcmp(kernel, "linux")==0){
        for(i=0;i<2;i++){
            system("rm /var/lib/pacman/sync/*db");
            system("pacman -Sy nvidia");
        }
        exit_sucess
    }elif(strcmp(kernel, "linux-lts")==0){
        for(i=0;i<2;i++){
            system("rm /var/lib/pacman/sync/*db");
            system("pacman -Sy nvidia-lts");
        }
        exit_sucess
    }elif(strcmp(kernel, "dkms")==0){
        for(i=0;i<2;i++){
            system("rm /var/lib/pacman/sync/*db");
            system("pacman -Sy nvidia-dkms");
        }
        exit_sucess
    }else{
        exit_failure
    }
    exit_sucess
}
